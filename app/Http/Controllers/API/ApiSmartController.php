<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// TODO: dados do namespace FACADES com http não esta funcionando
// use Illuminate\Support\Facades\Http;



class ApiSmartController extends Controller
{
    /**
  * Metodo deutilização da API https://smarts-totem.s3-sa-east-1.amazonaws.com/code-challenge/customers.json.
  *
  * @api
  *
  * @return json
  */
    function index(Request $request) {
        // $invoice = Http::get('https://smarts-totem.s3-sa-east-1.amazonaws.com/code-challenge/customers.json')->json();
         /**
        * Dados externos.
        *
        * {@internal Os dados provenientes da função posuem origem externa, url: https://smarts-totem.s3-sa-east-1.amazonaws.com/code-challenge/customers.json}}
        *
        * @return json o retorno é em json.
        */
        $invoice = file_get_contents('https://smarts-totem.s3-sa-east-1.amazonaws.com/code-challenge/customers.json');
        return $invoice;
    }
}
