# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

TODO: Dados iniciais para confecção do arquivo readme
smart 

Ambiente do projeto Smart


--- Comandos

git clone https://rodrigoazevedogutierrez@bitbucket.org/rodrigoazevedogutierrez/smart.git

```html
cd smart

[rodrigo@lua smart (master)]$ composer update;

[rodrigo@lua smart (master)]$ npm install

[rodrigo@lua smart (master)]$sudo chmod 777 storage
```
Acessar o mysql via terminal,ou interface, caso seja pelo terminal utilize o camando abaixo: 
```html
mysql> CREATE DATABASE smartHome;
```
você teve ter o resultado:
```html
Query OK, 1 row affected (0,02 sec)
```

o camando abaixo confirmara se a base que foi selecionada para ciração esta presente dentro do schema
```html
mysql> show databases;
```

o retultado deve constar o nome do banco de dados que foi utilizado na instrução create , conforme abaixo:

```html
+----------------------+
| Database             |
+----------------------+
| information_schema   |
| mysql                |
| performance_schema   |
| smar                 |
| smartHome            |
| sys                  |
+----------------------+
11 rows in set (0,00 sec)
```

Após a criação da base de dados éimportante copiar o arquivo .env.exemplo e um novo arquivo dentro do mesmo diretorio com o nome de .env.

Assim prosseguir com analise do conteudo desse arquivo. Deve ser localizado os dados abaixo e alterados conforme configurações do seu servidor mysql.

```html
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=[NOME DA BASE DE DADOS]
DB_USERNAME=[USUARIO]
DB_PASSWORD=[SENHA]
```
**Obs.: os nomes entre colchetes devem ser substituidos.

```html
[rodrigo@lua smart (master)]$ php artisan migrate
Migration table created successfully.
Migrating: 2014_10_12_000000_create_users_table
Migrated:  2014_10_12_000000_create_users_table
Migrating: 2014_10_12_100000_create_password_resets_table
Migrated:  2014_10_12_100000_create_password_resets_table
Migrating: 2016_06_01_000001_create_oauth_auth_codes_table
Migrated:  2016_06_01_000001_create_oauth_auth_codes_table
Migrating: 2016_06_01_000002_create_oauth_access_tokens_table
Migrated:  2016_06_01_000002_create_oauth_access_tokens_table
Migrating: 2016_06_01_000003_create_oauth_refresh_tokens_table
Migrated:  2016_06_01_000003_create_oauth_refresh_tokens_table
Migrating: 2016_06_01_000004_create_oauth_clients_table
Migrated:  2016_06_01_000004_create_oauth_clients_table
Migrating: 2016_06_01_000005_create_oauth_personal_access_clients_table
Migrated:  2016_06_01_000005_create_oauth_personal_access_clients_table
```

Obtendo sucesso na migração das tabelas é preciso gerar a chave de acesso que irá permitir o transito com OAuth2.0 a base de dados e a solução.

```html
[rodrigo@lua smart (master)]$ php artisan key:generate
Application key set successfully.
```
É preciso gerar as chaves no diretorio de storage do laravel, para garantir a comunicação segura entre a aplicação e a api (interna) de operações na base de dados.

```html
[rodrigo@lua smart (master)]$ php artisan passport:install
Encryption keys generated successfully.
Personal access client created successfully.
Client ID: 1
Client secret: AprwVflziYbmRHzdvk8qnVONHSlUgrReabQ06FTg
Password grant client created successfully.
Client ID: 2
Client secret: 2DVTY5l4nlYxciYdpC32vpT7pkc32ESrmZl73e4b
```
Após a instalação deve ser iniciada a aplicação com servidor do proprio php, dentro do laravel utilizando o artisan com o parametro serve.
```html
[rodrigo@lua smart (master)]$ php artisan serve 
```